#! /usr/bin/env node

console.log('This script populates 3dmodels to your database. Specified database as argument - e.g.: populatedb mongodb://your_username:your_password@your_dabase_url');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith('mongodb://')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}

var async = require('async')
var Model3d = require('./models/model3d')

var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var model3ds = []

function model3dCreate(model_name, material, colour, status, cb) {
  model3ddetail = {model_name: model_name , material: material, colour: colour }
  if (status != false) model3ddetail.status = status
  
  var model3d = new Model3d(model3ddetail);
       
  model3d.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New model3d: ' + model3d);
    model3ds.push(model3d)
    cb(null, model3d)
  }  );
}

function createModel3ds(cb) {
    async.parallel([
        function(callback) {
          model3dCreate('Kukkakorvikset', 'PLA', 'Keltainen', 'In store', callback);
        },
        function(callback) {
          model3dCreate('Sydänkorvikset', 'PLA', 'Punainen', 'In store', callback);
        },
		function(callback) {
          model3dCreate('KasipalloKalvosinnapit', 'PLA', 'Musta', 'In queue', callback);
        },
		function(callback) {
          model3dCreate('Tölkkikahva', 'PLA', 'Punainen', 'In store', callback);
        },
		function(callback) {
          model3dCreate('Holkki', 'PLA', 'Harmaa', 'In design', callback);
        },
		function(callback) {
          model3dCreate('Hyllynkannake', 'PLA', 'Valkoinen', 'In queue', callback);
        }
        ],
        // optional callback
        cb);
}

async.series([
    createModel3ds
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    // All done, disconnect from database
    mongoose.connection.close();
});