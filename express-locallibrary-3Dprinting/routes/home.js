var express = require('express');
var router = express.Router();

// Require controller modules.
var model3d_controller = require('../controllers/model3dController');

/// model3d ROUTES ///

// GET home page.
router.get('/', model3d_controller.index);

// GET request for creating a model3d. NOTE This must come before routes that display model3d (uses id).
router.get('/model3d/create', model3d_controller.model3d_create_get);

// POST request for creating model3d.
router.post('/model3d/create', model3d_controller.model3d_create_post);

// GET request to delete model3d.
router.get('/model3d/:id/delete', model3d_controller.model3d_delete_get);

// POST request to delete model3d.
router.post('/model3d/:id/delete', model3d_controller.model3d_delete_post);

// GET request to update model3d.
router.get('/model3d/:id/update', model3d_controller.model3d_update_get);

// POST request to update model3d.
router.post('/model3d/:id/update', model3d_controller.model3d_update_post);

// GET request for one model3d.
router.get('/model3d/:id', model3d_controller.model3d_detail);

// GET request for list of all model3d items.
router.get('/model3ds', model3d_controller.model3d_list);

module.exports = router;