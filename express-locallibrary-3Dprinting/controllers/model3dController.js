const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var Model3d = require('../models/model3d');
var async = require('async');

exports.index = function(req, res) {
    async.parallel({
        model3d_count: function(callback) {
            Model3d.count({}, callback); // Pass an empty object as match condition to find all documents of this collection
        },
        model3d_store_count: function(callback) {
            Model3d.count({status:'In store'}, callback);
        },
		model3d_queue_count: function(callback) {
            Model3d.count({status:'In queue'}, callback);
        },
		model3d_design_count: function(callback) {
            Model3d.count({status:'In design'}, callback);
        },
		model3d_unavailable_count: function(callback) {
            Model3d.count({status:'Unavailable'}, callback);
        },
    }, function(err, results) {
        res.render('index', { title: 'Print Mill', error: err, data: results });
    });
};

// Display list of all Models.
exports.model3d_list = function(req, res, next) {

  Model3d.find()
//    .sort([['model_name', 'ascending']])
	.populate('model3d')
    .exec(function (err, list_model3ds) {
      if (err) { return next(err); }
      //Successful, so render
      res.render('model3d_list', { title: '3D Models:', model3d_list: list_model3ds });
    });

};

// Display detail page for a specific Model3d.
exports.model3d_detail = function(req, res, next) {

    async.parallel({
        model3d: function(callback) {
            Model3d.findById(req.params.id)
              .exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.model3d==null) { // No results.
            var err = new Error('Model3d not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('model3d_detail', { title: 'Model3d Detail', model3d: results.model3d} );
    });

};

// Display Model3d create form on GET.
exports.model3d_create_get = function(req, res) {
    res.render('model3d_form', { title: 'Create 3D model'});
};

exports.model3d_create_post = [

    // Validate fields.
    body('model_name').isLength({ min: 1 }).trim().withMessage('Model name must be specified.')
        .isAlphanumeric().withMessage('Model name has non-alphanumeric characters.'),
    body('material').isLength({ min: 1 }).trim().withMessage('Material must be specified (PLA or ABS?).')
        .isAlphanumeric().withMessage('Material has non-alphanumeric characters.'),
	body('colour').isLength({ min: 1 }).trim().withMessage('Colour must be specified.')
        .isAlphanumeric().withMessage('Colour has non-alphanumeric characters.'),

    // Sanitize fields.
    sanitizeBody('model_name').trim().escape(),
    sanitizeBody('material').trim().escape(),
	sanitizeBody('colour').trim().escape(),
	sanitizeBody('status').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('model3d_form', { title: 'Create 3D model', model3d: req.body, errors: errors.array() });
            return;
        }
        else {
            // Data from form is valid.

            // Create an 3D model object with escaped and trimmed data.
            var model3d = new Model3d(
                {
                    model_name: req.body.model_name,
                    material: req.body.material,
					colour: req.body.colour,
                    status: req.body.status
                });
            model3d.save(function (err) {
                if (err) { return next(err); }
                // Successful - redirect to new model record.
                res.redirect(model3d.url);
            });
        }
    }
];

// Display Model3d delete form on GET.
exports.model3d_delete_get = function(req, res) {
	
    async.parallel({
        model3d: function(callback) {
            Model3d.findById(req.params.id).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.model3d==null) { // No results.
            res.redirect('/home/model3ds');
        }
        // Successful, so render.
        res.render('model3d_delete', { title: 'Delete 3D model', model3d: results.model3d} );
    });

};

// Handle Model3d delete on POST.
exports.model3d_delete_post = function(req, res) {
	
    async.parallel({
        model3d: function(callback) {
          Model3d.findById(req.body.model3did).exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        else {
            // Delete object.
            Model3d.findByIdAndRemove(req.body.model3did, function deleteModel3d(err) {
                if (err) { return next(err); }
                // Success - go to model list
                res.redirect('/home/model3ds')
            })
        }
    });
};

// Display Model3d update form on GET.
exports.model3d_update_get = function(req, res) {
    // Get model3d, authors and genres for form.
    async.parallel({
        model3d: function(callback) {
            Model3d.findById(req.params.id).populate('model3d').exec(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.model3d==null) { // No results.
                var err = new Error('Model not found');
                err.status = 404;
                return next(err);
            }
            // Success.
            res.render('model3d_form', { title: 'Update Model3d', model3ds:results.model3ds});
        });
};

// Handle Model3d update on POST.
exports.model3d_update_post = [
   
    // Validate fields.
    body('model_name', 'Model name must not be empty.').isLength({ min: 1 }).trim(),
    body('material', 'Material must not be empty.').isLength({ min: 1 }).trim(),
    body('colour', 'Colour must not be empty.').isLength({ min: 1 }).trim(),
    body('status', 'Status must not be empty').isLength({ min: 1 }).trim(),

    // Sanitize fields.
    sanitizeBody('model_name').trim().escape(),
    sanitizeBody('material').trim().escape(),
    sanitizeBody('colour').trim().escape(),
    sanitizeBody('status').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Model3d object with escaped/trimmed data and old id.
        var model3d = new Model3d(
          { model_name: req.body.model_name,
            material: req.body.material,
            colour: req.body.colour,
            status: req.body.status,
            _id:req.params.id //This is required, or a new ID will be assigned!
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all model3ds for form.
            async.parallel({
                model3ds: function(callback) {
                    Model3d.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                // Mark our selected genres as checked.
                
                res.render('model3d_form', { title: 'Update Model3d',model3ds:results.model3ds, errors: errors.array() });
            });
            return;
        }
        else {
            // Data from form is valid. Update the record.
            Model3d.findByIdAndUpdate(req.params.id, model3d, {}, function (err,themodel) {
                if (err) { return next(err); }
                   // Successful - redirect to model3d detail page.
                   res.redirect(themodel.url);
                });
        }
    }
];