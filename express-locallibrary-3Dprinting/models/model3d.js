var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Model3dSchema = new Schema(
  {
    model_name: {type: String, required: true, max: 100},
    material: {type: String, required: true, max: 100},
	colour: {type: String, required: true, max: 100},
	status: {type: String, required: true, enum: ['In store', 'In queue', 'In design', 'Unavailable'], default: 'Unavailable'},
  }
);

// Virtual for model3d's URL
Model3dSchema
.virtual('url')
.get(function () {
  return '/home/model3d/' + this._id;
});

//Export model
module.exports = mongoose.model('model3d', Model3dSchema);