var express = require('express');
var router = express.Router();

/* GET cool listing. */
router.get('/users/cool', function(req, res, next) {
  res.send('You\'re so cool');
});

module.exports = router;